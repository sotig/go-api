package home

import (
	"io"
	"net/http"
	"responses/json_response"
)

func Index(w http.ResponseWriter, req *http.Request) {
	type User struct {
		Name string `json:"name"`
	}

	user := &User{Name: "SOTIRIS"}

	json_response.WriteJSON(w, &user)
}

func Post(w http.ResponseWriter, req *http.Request) {
	type User struct {
		Name string `json:"name"`
	}

	user := &User{Name: "Frank"}

	json_response.WriteJSON(w, &user)
}

func HelloServer(w http.ResponseWriter, req *http.Request) {
	name := req.URL.Query().Get(":name")

	io.WriteString(w, "hello, "+name+"!\n")
}
