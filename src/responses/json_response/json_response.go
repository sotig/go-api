package json_response

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

func WriteJSON(w http.ResponseWriter, inStruct interface{}) {
	var jsonData []byte
	jsonData, err := json.Marshal(inStruct)
	if err != nil {
		fmt.Println(err)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	io.WriteString(w, string(jsonData))
}
